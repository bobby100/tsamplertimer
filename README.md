# TSamplerTimer

Lazarus component for specific time-based event triggering

This component is based on TIdleTimer and TLongTimer.
There was a request for data-logging every 15 minutes, starting with minute 00 on the clock
(12:00, 12:15, 12:30, 12:45, 13:00 etc.)

The interval can be set to 5, 10, 15, 20, 30 and 60 minutes.