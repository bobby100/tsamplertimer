unit SamplerTimer;

{ TSamplerTimer

  Based on TIdleTimer and TLongTimer components

  Copyright (C)2022 spasic@gmail.com
  Modified GPL
  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, LResources;

type
  TIntervalType = (it05, it10, it15, it20, it30, it60);

  TSamplerTimer = class(TCustomIdleTimer)
  private
    { Private declarations }
    fCurrentDateTime, fLastFiredDateTime: TDateTime;
    fIntervalType: TIntervalType;
    fVersion: string;
    fOnSample: TNotifyEvent;
  protected
    { Protected declarations }
    //procedure DoOnIdle(Sender: TObject; var Done: boolean); override;
    //procedure DoOnIdleEnd(Sender: TObject); override;
    procedure DoOnTimer; override;
  public
    { Public declarations }
    constructor Create(TheOwner: TComponent); override;
    // Until the first Timer event, this will be the component's creation time
    property LastFired: TDateTime read fLastFiredDateTime;
  published
    { Published declarations }
   { // Default=false
    property AutoEnabled;
    // Same as TIdleTimer
    property AutoStartEvent;
    // Same as TIdleTimer
    property AutoEndEvent;}

    // Same as TIdleTimer
    property Enabled;
    // This is fired only at the Intervals you set
    property OnTimer;
   { // Same as TIdleTimer
    property OnStartTimer;
    // Same as TIdleTimer
    property OnStopTimer; }

    // the OnTimer event will be fired
    // it05 - on every 5 minutes (e.g. 12:00, 12:05, 12:10)
    // it15 - on every 15 minutes (e.g. 14:00, 14:15, 14:30, 14:45 etc.)
    // etc.
    // the interval is relative to the 00 minutes on the clock, not to the timer start/enable
    property IntervalType: TIntervalType read fIntervalType
      write fIntervalType default it15;
    property Version: string read fVersion;
    // Fired every time SamplerTimer samples
    property OnSample: TNotifyEvent read fOnSample write fOnSample;
  end;

procedure Register;

implementation

const
  C_OneMinute = 60000;
  C_Version = '0.0.1.1';

(*
  V0.0.1: Initial commit
*)
procedure Register;
begin
  RegisterComponents('System', [TSamplerTimer]);
  {$I samplertimer_icon.lrs}
end;

constructor TSamplerTimer.Create(TheOwner: TComponent);
begin
  inherited;
  fCurrentDateTime := Now;
  fLastFiredDateTime := Now;

  // Set defaults for properties
  Interval := C_OneMinute;
  fIntervalType := it15;
  fVersion := C_Version;
end;

{procedure TSamplerTimer.DoOnIdle(Sender: TObject; var Done: boolean);
begin
  // Do nothing special here
  inherited;
end;

procedure TSamplerTimer.DoOnIdleEnd(Sender: TObject);
begin
  // Do nothing special here
  inherited;
end;     }

procedure TSamplerTimer.DoOnTimer;
// Only allow this event to fire ONCE if datetime matches the interval set
var
  cD, cM, cY, cH, cMi, cS, cms: word;
  lD, lM, lY, lH, lMi, lS, lms: word;
  fire: boolean;
begin
  fire := False;
  // Split Current date into parts
  fCurrentDateTime := Now;
  DecodeDate(fCurrentDateTime, cY, cM, cD);
  DecodeTime(fCurrentDateTime, cH, cMi, cS, cmS);

  // Split LastFired date into parts
  DecodeDate(fLastFiredDateTime, lY, lM, lD);
  DecodeTime(fLastFiredDateTime, lH, lMi, lS, lmS);

  if (fIntervalType = it05) then
    if (cMi in [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]) then
      fire := True;

  if (fIntervalType = it10) then
    if (cMi in [0, 10, 20, 30, 40, 50]) then
      fire := True;

  if (fIntervalType = it15) then
    if (cMi in [0, 15, 30, 45]) then
      fire := True;

  if (fIntervalType = it20) then
    if (cMi in [0, 20, 40]) then
      fire := True;

  if (fIntervalType = it30) then
    if (cMi in [0, 30]) then
      fire := True;

  // Fire the OnSample event?
  if Assigned(fOnSample) then
    fOnSample(Self);

  //get sure that the event don't get fired twice in one minute. Should not be possible anyway, but who knows...
  if (lY = cY) and (lM = cM) and (ld = cD) and (lH = cH) and (lMi = cMi) then
    fire := False;

  // Fire the OnTimer event for the user
  if fire then
  begin
    inherited; // Do whatever the user wants done
    fLastFiredDateTime := Now;// Record the DateTime the OnTimer was fired
  end;
end;

end.
