{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit SamplerTimer_Package;

{$warn 5023 off : no warning about unused units}
interface

uses
  SamplerTimer, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('SamplerTimer', @SamplerTimer.Register);
end;

initialization
  RegisterPackage('SamplerTimer_Package', @Register);
end.
